/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pooexu3;

/**
 *
 * @author Jesse
 */
public class Examen {

	public static boolean esPrimo(int n) {
		int divisores = 0;
		if (n == 1) {
			return false;
		}
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				divisores += 1;
			}
		}

		return divisores <= 2;
	}

	public static void primosAnteriores(int n) {
		System.out.println("Lista de números primos anteriores a"
				+ ": " + n);
		for (int i = 2; i < n; i++) {
			if (esPrimo(i)) {
				System.out.println(i);
			}
		}
	}

	public static String sumaEstándar(String s1, String s2) {
		String r = "";

		String longest;
		String shortest;

		if (s1.length() >= s2.length()) {
			longest = s1;
			shortest = s2;
		} else {
			longest = s2;
			shortest = s1;
		}

		if (longest.length() != shortest.length()) {
			shortest = fixString(shortest, (longest.length() - shortest.length()));
		}

		int residuo;
		int cociente = 0;
		int x, y;
		int suma;
		for (int i = longest.length() - 1; i >= 0; i--) {
			x = Integer.parseInt(Character.toString(longest.charAt(i)));
			y = Integer.parseInt(Character.toString(shortest.charAt(i)));

			suma = x + y + cociente;
			cociente = (suma) / 10;
			residuo = suma % 10;

			r = Integer.toString(residuo).concat(r);

			if (i == 0) {
				r = Integer.toString(cociente).concat(r);
			}
		}
		
		if (r.charAt(0) == '0') {
			r = r.substring(1, r.length());
		}

		return r;
	}

	private static String fixString(String s, int digits) {
		String fixedString;

		String zeroes = "";
		for (int i = 0; i < digits; i++) {
			zeroes += "0";
		}

		fixedString = zeroes.concat(s);

		return fixedString;
	}
}
