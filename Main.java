/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pooexu3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Jesse
 */
public class Main {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int x;
		try {
			System.out.print("Ingrese un entero: ");
			x = reader.nextInt();

			System.out.print("El número ");
			if (Examen.esPrimo(x)) {
				System.out.print("es");
			} else {
				System.out.print("no es");
			}
			
			System.out.println(" primo.");
			
			Examen.primosAnteriores(x);

		} catch (InputMismatchException e) {
			System.out.println("Debe ingresar un entero.");
		}
		
		System.out.println("Ingrese el primer número: ");
		String n1 = reader.nextLine();
		System.out.println("Ingrese el segundo número: ");
		String n2 = reader.nextLine();
		
		String r = Examen.sumaEstándar(n1, n2);
		
		System.out.println("El resultado de la suma es: " + r);

	}
}
